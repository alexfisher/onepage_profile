api = 2
core = 7.x

; Core
projects[] = drupal

; Contrib Modules
projects[context][subdir] = contrib
projects[ctools][subdir] = contrib
projects[devel][subdir] = contrib
projects[diff][subdir] = contrib
projects[features][subdir] = contrib
projects[features_extra][subdir] = contrib
projects[strongarm][subdir] = contrib
projects[views][subdir] = contrib
projects[module_filter][subdir] = contrib
projects[admin_menu][subdir] = contrib
projects[token][subdir] = contrib
projects[pathauto][subdir] = contrib
projects[metatag][subdir] = contrib
projects[block_class][subdir] = contrib
projects[entity][subdir] = contrib
projects[globalredirect][subdir] = contrib
projects[adminimal_admin_menu][subdir] = contrib
projects[jquery_update][subdir] = contrib
projects[empty_front_page][subdir] = contrib
projects[draggableviews][subdir] = contrib
projects[coder][subdir] = contrib

; Custom modules
projects[custom][download][type] = "git"
projects[custom][download][url] = "https://tomgeekery@bitbucket.org/alexfisher/compro_custom.git"
projects[custom][type] = "module"
projects[custom][subdir] = "custom"

projects[orbit_views][download][type] = "git"
projects[orbit_views][download][url] = "https://tomgeekery@bitbucket.org/tomgeekery/orbit_views.git"
projects[orbit_views][type] = "module"
projects[orbit_views][subdir] = "custom"

projects][onepage_feature][download][type] = "git"
projects[onepage_feature][download][url] = "https://tomgeekery@bitbucket.org/alexfisher/onepage_feature.git"
projects[onepage_feature][type] = "module"
projects[onepage_feature][subdir] = "features"

; Profile
projects[onepage_profile][type] = profile
projects[onepage_profile][download][type] = git
projects[onepage_profile][download][url] = "https://tomgeekery@bitbucket.org/alexfisher/onepage_profile.git"

; Themes
projects[] = adminimal_theme
projects[zurb-foundation][patch][2205041] = https://drupal.org/files/issues/zurb-foundation-array-2-string-2205041-6.patch

projects[onepage_theme][type] = theme
projects[onepage_theme][download][type] = git
projects[onepage_theme][download][url] = "https://tomgeekery@bitbucket.org/alexfisher/onepage_theme.git"
