<?php

/**
 * @file
 * 
 */

/**
 * Implements hook_install_tasks().
 */
function onepage_profile_install_tasks($install_state) {
  $tasks['placeholder_content'] = array(
    'display_name' => st('Create placeholder content'),
    'display' => TRUE,
    'type' => 'normal',
    'run' => INSTALL_TASK_RUN_IF_REACHED,
    'function' => 'onepage_profile_placeholder_content',
  );
  
  return $tasks;
}

/**
 * Creates placeholder content.
 */
function onepage_profile_placeholder_content() {
  // Create some starter content.
	$sections = 2;
  $slides = 2;
	
	// Include devel_generate for creating random text.
  module_load_include('inc', 'devel_generate', 'devel_generate');
  
	// Create sections
	for ($i = 1; $i <= $sections; $i++) {
		$node = new stdClass();
		$node->type = 'section';
		$node->title = devel_create_greeking(2, TRUE);
		$node->language = LANGUAGE_NONE;
		$node->path = array('alias' => 'section1');
		node_object_prepare($node);
		$node->uid = 1;
		// Populate body field
		$node->body[$node->language][0]['value'] = devel_create_greeking(200);
		// Populate image field.
		$file_path = drupal_get_path('theme', 'onepage_theme') . '/images/400x200.png';
		$file = (object) array(
		  'uid' => 1,
		  'uri' => $file_path,
		  'filemime' => file_get_mimetype($file_path),
		  'status' => 1,
		);
		$file = file_copy($file, 'public://');
		$node->field_section_image[LANGUAGE_NONE][0] = (array)$file;
		// Save the node
		$node = node_submit($node);
		node_save($node);
	}
	
	// Create slides
	for ($i = 1; $i <= $slides; $i++) {
		$node = new stdClass();
		$node->type = 'slide';
		$node->title = devel_create_greeking(2, TRUE);
		$node->language = LANGUAGE_NONE;
		node_object_prepare($node);
		$node->uid = 1;
		// Populate caption field
		$node->field_slide_caption[$node->language][0]['value'] = devel_create_greeking(50);
		// Populate image field.
		$file_path = drupal_get_path('theme', 'onepage_theme') . '/images/1920x500.png';
		$file = (object) array(
		  'uid' => 1,
		  'uri' => $file_path,
		  'filemime' => file_get_mimetype($file_path),
		  'status' => 1,
		);
		$file = file_copy($file, 'public://');
		$node->field_slide_image[LANGUAGE_NONE][0] = (array)$file;
		// Save the node
		$node = node_submit($node);
		node_save($node);
	}
}
